export let StandarFirmas = function (doc, obj, nota,  pos) {

    /** Calculamos las posiciones de imagen, firma y nota**/
    let posLinea = pos + 3;
    let poslabel = pos + 7;
    let posNota = pos + 12;
    let posImage = pos + 20;
    let posFirma = pos + 50;

    doc.line(10, posLinea, 200, posLinea);
    doc.text('Notas: ', 10, poslabel);
    let splitTitle = doc.splitTextToSize(nota, 180);
    doc.text(10, posNota, splitTitle);

    // Agregamos la primera imagen obligada
    doc.addImage(obj[0].signature2, 'JPEG', 70, posImage);
    doc.text(75, posFirma, "Validado por: " + obj[0].dr_name);

    // Calulamos si existe la segunda firma, y la imprimimos
    let secund = obj[1];
    if (typeof secund !== 'undefined') {
        doc.addImage(secund.signature2, 'JPEG', 140, posImage);
        doc.text(145, posFirma, "Validado por: " + secund.dr_name);
    }

};

