export let DateFormat = function (date) {
    let fecha = new Date(date);
    fecha = new Intl.DateTimeFormat('en-AU').format(fecha);
    return fecha;
};