import {
    footer
} from "./Images";

export var SubFooter = function (doc) {
    doc.addImage(footer, 'png', 0, 267, 210, 30);
};