import {
    DateFormat
} from "./DateFormat";

export let StandarHeader = function (doc, obj) {
    // Se construye la cabezea del examen
    doc.rect(10, 60, 190, 10); // empty square
    doc.setFontSize(10);
    //doc.text(75, 67, obj.nombre, null, null, 'center');
    let xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(obj.nombre) / 2);
    doc.text(obj.nombre, xOffset, 67, null, null, 'center');

    doc.setFontSize(10);
    doc.text(10, 80, 'Orden de Trabajo: ' + obj.id_orden);

    if (obj.fecha != null) {
        doc.text(70, 80, 'Fecha: ' + DateFormat(obj.fecha.from));
    }

    doc.text(115, 80, 'Area: ' + obj.area);

    doc.text(155, 80, 'Subarea: ' + obj.subarea);

};