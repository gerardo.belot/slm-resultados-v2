import * as jsPDF from 'jspdf';
import store from '../store/store';
import {
    Standart
} from "./Standar";
import {
    Curva
} from "./Curva";
import {
    Render
} from './pdfRender';

export function CreatePdf() {

    // Llamamos a la libreria de js-PDF
    let doc = new jsPDF();

    // Llamanos el objeto de Ordenes desde el STORE en estdos globales
    let obj = store.getters.getOrdenDetail;

    //Creamos el objeto con examenes que no son curvas
    let examenes = obj.examenes.filter(examenes => examenes.is_curva !== true);

    // Creamos objeto solo con curvas
    let onCurvas = obj.examenes.filter(curvas => curvas.is_curva === true);

    // Pasamos el objeto de examenes al loop de examenes Standart
    examenes.forEach(function (examen) {
        Standart(doc, examen);
    });

    // Pasamos el objeto con curvas si existen
    if (onCurvas.length) {
        Curva(doc, onCurvas);
    }

    Render(doc);

}