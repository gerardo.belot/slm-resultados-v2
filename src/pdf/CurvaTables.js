import {
    CurvaTableBody
} from "./CurvaTablesBody";
import {
    CurvaTableFirmas
} from "./CurvaTableFirmas";
import {
    CurvaImagen
} from "./CurvaImage";

export let CurvaTables = function (doc, obj) {

    doc.rect(10, 60, 190, 10); // empty square
    doc.setFontSize(15);
    doc.text(75, 67, "Grafica de Resultados");
    doc.setFontSize(10);

    let secund = obj[1];
    if (typeof secund !== 'undefined') {
        doc.line(10, 84, 100, 84);

        doc.text(10, 90, "Horas");
        doc.text(40, 90, obj[0].resultados.data[0].unit);
        doc.text(70, 90, "Valor Referencia");

        doc.line(10, 95, 100, 95);

        let tableOnePos = CurvaTableBody(doc, obj[0].resultados.data, 1, {
            a: 10,
            b: 40,
            c: 70
        }, 90);
        CurvaTableFirmas(doc, obj[0].validations, tableOnePos, 30);

        doc.line(110, 84, 200, 84);
        doc.text(110, 90, "Horas");
        doc.text(140, 90, obj[1].resultados.data[0].unit);
        doc.text(170, 90, "Valor Referencia");

        doc.line(110, 95, 200, 95);

        let tableTwoPos = CurvaTableBody(doc, obj[1].resultados.data, 1, {
            a: 110,
            b: 140,
            c: 170
        }, 90);
        CurvaTableFirmas(doc, obj[1].validations, tableTwoPos, 130);
        CurvaImagen(doc, obj[0].resultados);

    } else {

        doc.line(10, 84, 200, 84);

        doc.text(30, 90, "Horas");
        doc.text(95, 90, obj[0].resultados.data[0].unit);
        doc.text(150, 90, "Valor Referencia");

        doc.line(10, 95, 200, 95);
        let tableOnePos = CurvaTableBody(doc, obj[0].resultados.data, 2, {
            a: 20,
            b: 95,
            c: 150
        }, 90);
        CurvaTableFirmas(doc, obj[0].validations, tableOnePos, 80);
        CurvaImagen(doc, obj[0].resultados);
    }

};