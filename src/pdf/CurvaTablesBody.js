export let CurvaTableBody = function (doc, obj, type, alingCols, alingRow) {

    let trans = alingRow;
    let i = 1;
    let repet = obj.length;
    let pos = (repet * 10) + 100;

    obj.forEach((rows) => {
        var J = 10;
        var R = i++;
        var rest = J * R;
        var fila = trans + rest;


        doc.text(alingCols.b, fila, rows.value.toString());
        doc.text(alingCols.c, fila, rows.valref);
    });

    return pos;


};