import {
    SubHeader
} from "./Header";
import {
    SubFooter
} from "./Footer";
import {
    StandarHeader
} from "./StandarHeader";
import {
    StandarTable
} from "./StandarTable";

export var Standart = function (doc, obj) {

    // Creamos la primera pagina
    doc.addPage();

    // Llamamos a la cabezera de todos los examenes
    SubHeader(doc);

    // Llamamos a la cabezera de cada examen, pasamos obj como examen
    StandarHeader(doc, obj);

    // Aquí se construye el cuerpo de cada tabla
    StandarTable(doc, obj);

    // Se llama a la imagen de footer
    SubFooter(doc);

};