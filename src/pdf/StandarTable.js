import {
    StandarFirmas
} from "./StandarFirma";
import {copia} from './copia'


export let StandarTable = function (doc, obj) {

    doc.addImage(copia, 'PNG', 10, 15, 60, 15);
    //doc.text(50, 'COPIA', 10, 15, 60, 15)
    //agregamos config de letra
    doc.setFontSize(8)

    // Se construye el cuerpo del examen
    doc.line(10, 84, 200, 84);

    doc.text(10, 90, "Nombre");
    doc.text(75, 90, "Resultados");
    doc.text(130, 90, "Unidad");
    doc.text(170, 90, "Valor Referencia");

    doc.line(10, 95, 200, 95);

    // Se da valor a la posicion X del la tabla
    let trans = 100;
    let i = 1;

    // se calcula cual la cantidad de filas
    let repet = obj.resultados.data.length;

    // Obtenemos el tamaño final del total de la tabla
    let posicion = (repet * 7) + 100;

    // eslint-disable-next-line
    for (let [index, value] of Object.entries(obj.resultados.data)) {
        /**  Calculamos el espaciado entre filas **/
        var J = 6;
        var R = i++;
        var rest = J * R;
        var fila = trans + rest;

        /*****************/
        // Construimos la tabla
        doc.text(10, fila, value.title);
        doc.text(75, fila, value.value.toString());
        doc.text(130, fila, value.unit);
        doc.text(170, fila, value.valref);
    }

    /** Agregamos la firma en la posicion calcula del final de la tabla */
    StandarFirmas(doc, obj.validations, obj.nota, posicion);

};