export let CurvaTableFirmas = function (doc, obj, pos, lat) {

    /** Calculamos las posiciones de imagen y firma **/
    let posImage = pos + 5;
    let posFirma = pos + 30;

    doc.addImage(obj[0].signature2, 'JPEG', lat, posImage);
    doc.text(lat, posFirma, "Validado por: " + obj[0].dr_name);

};