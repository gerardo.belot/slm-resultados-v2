
export let Render = function (doc){
    doc.deletePage(1);
    //doc = addWaterMark(doc);
    var string = doc.output('dataurl');
    var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>";
    var x = window.open();
    x.document.open();
    x.document.write(iframe);
    x.document.close();
}

function addWaterMark(doc) {
  var totalPages = doc.internal.getNumberOfPages();
  let i;
  for (i = 1; i <= totalPages; i++) {
    doc.setPage(i);
    //doc.addImage(imgData, 'PNG', 40, 40, 75, 75);
    doc.setTextColor(150);
    doc.text(50, doc.internal.pageSize.height - 30, 'Watermark');
  }

  return doc;
}