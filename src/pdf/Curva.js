import {
    SubFooter
} from "./Footer";
import {
    SubHeader
} from "./Header";
import {
    CurvaTables
} from "./CurvaTables";

export var Curva = function (doc, obj) {

    doc.addPage();

    SubHeader(doc);

    CurvaTables(doc, obj);

    SubFooter(doc);
};