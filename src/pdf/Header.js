import {
    header, copy
} from "./Images";
import store from '../store/store';
import {
    DateFormat
} from "./DateFormat";
import moment from 'moment';

export var SubHeader = function (doc) {

    let pageInfo = doc.internal.getCurrentPageInfo();

    /**
     * Debido a que estamos dentro del loop de examenes, invocamos desde
     * Store el estado de la ORDEN para imprimir los datos de la cabezera
     * */
    let orden = store.getters.getOrdenDetail;

    const edad = moment().diff(orden.pacientes.birth, 'years', false)

    /**
     * construimos la cabezera de los examenes
     */
    doc.addImage(header, 'png', 80, 5, 200, 25);

    /**
     * Agreganos imagen de copia   
     */
     //doc.addImage(copy, 'png', 80, 5, 200, 25);

    if (pageInfo.pageNumber === 2) {
        // Informe
        doc.line(10, 33, 200, 33); // horizontal line
        doc.setFontSize(19);

        doc.text(60, 40, 'INFORME DE RESULTADOS');
        doc.line(10, 42, 200, 42); // horizontal line
    }

    doc.setFontSize(11);

    doc.setFontStyle('bold');
    doc.text(10, 49, "Paciente: ");
    doc.setFontStyle('normal');
    doc.text(28, 49, orden.pacientes.nombre);

    doc.setFontStyle('bold');
    doc.text(100, 49, "Sexo: ");
    doc.setFontStyle('normal');
    doc.text(112, 49, orden.pacientes.gender);

    doc.setFontStyle('bold');
    doc.text(130, 49, "Edad: ");
    doc.setFontStyle('normal');
    //doc.text(142, 49, orden.pacientes.id);
    doc.text(142, 49, edad.toString())
    //doc.text(142, 49, moment().diff(orden.pacientes.birth, 'years', false));

    doc.setFontStyle('bold');
    doc.text(158, 49, "Fecha Nac: ");
    doc.setFontStyle('normal');
    doc.text(180, 49, DateFormat(orden.pacientes.birth));

    doc.setFontStyle('bold');
    doc.text(10, 56, "Medico: ");
    doc.setFontStyle('normal');
    doc.text(26, 56, orden.medico);

    doc.setFontStyle('bold');
    doc.text(85, 56, "Sucursal: ");
    doc.setFontStyle('normal');
    doc.text(103, 56, orden.sucursal);

    doc.setFontStyle('bold');
    doc.text(114, 56, "Cod. interno: ");
    doc.setFontStyle('normal');
    doc.text(138, 56, orden.no_factura.toString());

    doc.setFontStyle('bold');
    doc.text(158, 56, "Fecha Fac: ");
    doc.setFontStyle('normal');
    doc.text(180, 56, DateFormat(orden.fecha_factura));
};