export default {
    LOGIN_SUCCESS(state, payload) {
        state.isLoggedIn = true
        state.token = payload.token
        state.user = payload.name
        state.user_type = payload.user_type; // cambiar de boolean a numerico
        state.user_id = payload.id
        localStorage.setItem('token', payload.token)
        localStorage.setItem('user', payload.name)
        localStorage.setItem('user_id', payload.id)
        localStorage.setItem('isLoggedIn', true)
        localStorage.setItem('user_type', payload.user_type)
    },

    LOGOUT(state) {
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        localStorage.removeItem('user_id')
        localStorage.removeItem('isLoggedIn')
        localStorage.removeItem('user_type')
        state.user_type = ''
        state.user_id = ''
        state.isLoggedIn = null
        state.usuario = null
    },

    SET_PACIENTES_LIST(state, payload) {
        state.pacientesList = payload
    },

    SET_PACIENTE(state, payload) {
        state.paciente = payload
    },

    SET_ORDEN(state, payload) {
        state.orden = payload
    },

    SET_LISTADOPACIENTES_SEARCH(state, payload) {
        state.listadoPacientesSearch = payload;
    },

    SET_RESULTADOS_FACTURAS_SEARCH(state, payload) {
        state.facturaSearch = payload;
    },


}