import axios from 'axios'
import server from '../server.js'

export default {

    GET_FACTURA_SEARCH: ({
        commit,
        getters
    }, param) => {
        return new Promise((resolve, reject) => {
            let string = `${server}/api/v1/labmed/admin/factura/${param}`
            axios.get(string, {
                headers: {
                    token: getters.getToken
                }
            }).then((resp) => {
                resolve(resp)
                commit('SET_RESULTADOS_FACTURAS_SEARCH', resp.data)
            }).catch(err => {
                reject(err)
            })
        })
    },

    GET_LISTADOS_PACIENTES_SEARCH: ({
        commit,
        getters
    }, param) => {
        return new Promise((resolve, reject) => {
            let string = `${server}/api/v1/labmed/admin/search/${param}`
            axios.get(string, {
                headers: {
                    token: getters.getToken
                }
            }).then((resp) => {
                resolve(resp)
                commit('SET_LISTADOPACIENTES_SEARCH', resp.data)
            }).catch(err => {
                reject(err)
                // eslint-disable-next-line
                console.log(err)
            })
        })
    },

    GET_ORDENES_DETAIL: ({
        commit,
        getters
    }, param) => {
        return new Promise((resolve, reject) => {
            let string = `${server}/api/v1/labmed/ordenes/${param}`
            axios.get(string, {
                headers: {
                    token: getters.getToken
                }
            }).then((resp) => {
                commit('SET_ORDEN', resp.data)
                resolve(resp)
            }).catch(err => {
                reject(err)
            })
        })
    },

    GET_PACIENTE_DETAIL: ({
        commit,
        getters
    }, param) => {
        return new Promise((resolve, reject) => {
            let string = ''
            if (param.search != '') {
                string = `${server}/api/v1/labmed/pacientes/detalles/${param.paciente}?search=${param.search}`
            } else {
                string = `${server}/api/v1/labmed/pacientes/detalles/${param.paciente}?page=${param.page}`
            }

            axios.get(string, {
                headers: {
                    token: getters.getToken
                }
            }).then(resp => {
                resolve(resp)
                commit('SET_PACIENTE', resp.data)
            }).catch(err => {
                reject(err)
            })

        })
    },

    GET_PACIENTES_LIST: ({
        commit,
        getters
    }, param) => {
        return new Promise((resolve, reject) => {

            let string = ''
            if (param.search != '') {
                string = `${server}/api/v1/labmed/pacientes/${param.user_id}?search=${param.search}&page=${param.page}`;
            } else {
                string = `${server}/api/v1/labmed/pacientes/${param.user_id}?page=${param.page}`;
            }
            axios.get(string, {
                headers: {
                    token: getters.getToken
                }
            }).then(resp => {
                resolve(resp)
                commit('SET_PACIENTES_LIST', resp.data)
            }).catch(err => {
                reject(err)
            })

        })
    },

    AUTH_REQUEST: ({
        commit
    }, params) => {
        return new Promise((resolve, reject) => {
            const string = `${server}/api/v1/login`
            axios.post(string, params)
                .then(resp => {
                    commit('LOGIN_SUCCESS', resp.data)
                    resolve(resp)
                }).catch(err => {
                    reject(err)
                })
        })
    }
}
