export default {
    token: localStorage.getItem('token'),
    user: localStorage.getItem('user'),
    user_id: localStorage.getItem('user_id'),
    isLoggedIn: JSON.parse(localStorage.getItem('isLoggedIn')),
    user_type: JSON.parse(localStorage.getItem('user_type')),
    pacientesList: [],
    paciente: [],
    orden: [],
    listadoPacientesSearch: [],
    facturaSearch: '',
}