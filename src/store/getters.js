export default {
    getUserId(state) {
        return state.user_id
    },
    getUserName(state) {
        return state.user
    },
    getLogStatus(state) {
        return state.isLoggedIn
    },
    getToken(state) {
        return state.token
    },
    getUserType(state) {
        return state.user_type
    },
    getListadoPacientes(state) {
        return state.pacientesList
    },
    getListadoOrdenes(state) {
        return state.paciente
    },
    getOrdenDetail(state) {
        return state.orden
    },
    getListadoPacientesSearch(state) {
        return state.listadoPacientesSearch
    },
    getFacturaSearch(state) {
        return state.facturaSearch;
    },

}