import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/dashboard/:id?/:search?',
      name: 'dashboard',
      component: () => import( /* webpackChunkName: "about" */ './views/admin/Dashboard.vue'),
      meta: {
        auth: true
      }
    },
    {
      path: '/admin/dashboard/byFactNum',
      name: 'AdminDashboardByFact',
      component: () => import( /* webpackChunkName: "adminDashboard" */ './views/admin/AdminDashboardByFact.vue'),
      meta: {
        auth: true,
        admin: true
      }
    },
    {
      path: '/admin/dashboard',
      name: 'AdminDashboard',
      component: () => import( /* webpackChunkName: "adminDashboard" */ './views/admin/AdminDashboard.vue'),
      meta: {
        auth: true,
        admin: true
      }
    },
    {
      path: '/paciente/:id/:search?/:page?',
      name: 'pacientesDetalle',
      component: () => import( /* webpackChunkName: "pacientesDetalle" */ './views/admin/PacientesDetail.vue'),
      meta: {
        auth: true
      }
    },
    {
      path: '/pacientes/ordenes/:orden_id',
      name: 'ordenesDetalle',
      component: () => import( /* webpackChunkName: "ordenesDetalle" */ './views/ordenes/OrdenesDetail.vue'),
      meta: {
        auth: true
      }
    }
  ]
})